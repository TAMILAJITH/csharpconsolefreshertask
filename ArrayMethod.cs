using System;
using System.Linq;

namespace ConsoleApp
{
    public static class ArrayMethod
    {
        // Compute the Sum in array

        static void sumOfArray()
        {

            int[] integerValue = { 1, 2, 2, 3, 3, 4, 5, 6, 5, 7, 7, 7, 8, 8, 1 };

            Console.WriteLine("Expected Output :\n");

            Console.WriteLine("Sum:" + integerValue.Sum());

        }

        //Get the larger value between first and last element of an array
        static void getLargestNo()
        {

            Console.Write("Please enter first Integer");
            int first = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please enter second Integer");
            int second = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please enter third Integer");
            int third = Convert.ToInt32(Console.ReadLine());

            int[] intArray = { first, second, third };

            int largestNo = 0;

            int valueOfNo = 0;

            foreach (int item in intArray)
            {

                if (item > valueOfNo)
                {
                    largestNo = item;

                    valueOfNo = item;
                }
            }
            Console.WriteLine("Expected Output :\n");

            Console.WriteLine("Highest value between first and last values of the said array :" + largestNo);
        }


        //  Check Array Contains Odd Number

        static void checkOddNoInArray()
        {

            Console.Write("Please enter first integer");
            int first = Convert.ToInt32(Console.ReadLine());
            Console.Write("Please enter second integer");
            int second = Convert.ToInt32(Console.ReadLine());
            Console.Write("Please enter third integer");
            int third = Convert.ToInt32(Console.ReadLine());
            Console.Write("Please enter fourth integer");
            int fourth = Convert.ToInt32(Console.ReadLine());
            Console.Write("Please enter fifth integer");
            int fifth = Convert.ToInt32(Console.ReadLine());

            int[] intArray = new int[5] { first, second, third, fourth, fifth };

            foreach (int item in intArray)
            {

                if (item % 2 != 0)
                {
                    Console.WriteLine("Expected Output :\n");

                    Console.WriteLine("Check if an array contains an odd number? " + item);
                    break;
                }
            }

        }

        //  2D array of size 3x3 and print the matrix.
        static void array2dMethod()
        {

            int[,] intArray = new int[3, 3];
            intArray[0, 0] = 1;

            for (int i = 0; i < 3; i++)
            {

                for (int j = 0; j < 3; j++)
                {
                    Console.Write("Element-[{0},{1}] =", i, j);

                    intArray[i, j] = Convert.ToInt32(Console.ReadLine());
                }

            }
            Console.WriteLine("Expected Output :\n");

            Console.WriteLine("The Matrix value is :\n");
            for (int i = 0; i < 3; i++)
            {

                Console.Write("\n");
                for (int j = 0; j < 3; j++)
                {
                    Console.Write("{0}\t", intArray[i, j]);
                }
            }

        }

        public static void loopAllMethods()
        {

            Console.WriteLine("Press 1 for Compute the Sum");
            Console.WriteLine("Press 2 for  get the larger value");
            Console.WriteLine("Press 3 for eck Array Contains Odd Number");
            Console.WriteLine("Press 4 for 2D array");
            Console.Write("Enter here :");
            int userChoice = Convert.ToInt32(Console.ReadLine());

            switch (userChoice)
            {

                case 1: sumOfArray(); break;
                case 2: getLargestNo(); break;
                case 3: checkOddNoInArray(); break;
                case 4: array2dMethod(); break;
            }
        }

    }
}