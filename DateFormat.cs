using System;


namespace ConsoleApp
{

    public static class DateFormat
    {

        public static void formatDate()
        {

            DateTime date = DateTime.Now;

            Console.WriteLine("Expected Output :\n");

            Console.WriteLine("Complete date:" + date.ToString());
            Console.WriteLine("Short Date:" + date.ToShortDateString());
            Console.WriteLine("Display date using 24-hour clock format:");
            Console.WriteLine(date.ToString());
            Console.WriteLine(date.ToString("MM/dd/yyyy HH:mm:ss"));

        }

        public static void seperateDateFormat()
        {

            DateTime date = DateTime.Now;

            Console.WriteLine("Expected Output :\n");

            Console.WriteLine("year =" + date.Year);
            Console.WriteLine("Month =" + date.Month);
            Console.WriteLine("Day =" + date.Day);
            Console.WriteLine("Hour =" + date.Hour);
            Console.WriteLine("Minute =" + date.Minute);
            Console.WriteLine("Seconds =" + date.Second);
            Console.WriteLine("MilliSeconds =" + date.Millisecond);

        }

    }
}