using System;

namespace ConsoleApp
{

    public class LoopingMethod
    {

        static void loopNumbers()
        {

            Console.WriteLine("Expected Output :\n");

            for (int i = 1; i <= 10; i++)
            {
                Console.Write(i + " ");

            }
        }

        static void sumLoopNumber()
        {
            Console.WriteLine("Output :\n");
            Console.Write("The first 10 natural number is :");

            int sumOfNo = 0;
            for (int i = 1; i <= 10; i++)
            {

                sumOfNo += i;
                Console.Write(i + " ");

            }
            Console.WriteLine();
            Console.WriteLine("Expected Output :\n");
            Console.WriteLine("The Sum is : {0}", sumOfNo);

        }

        static void showNoInRectang()
        {

            Console.Write("Enter the number :");
            int x = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Expected Output : \n");

            Console.WriteLine("{0}{0}{0}{0}", x);
            Console.WriteLine("{0}  {0}", x);
            Console.WriteLine("{0}  {0}", x);
            Console.WriteLine("{0}  {0}", x);
            Console.WriteLine("{0}{0}{0}{0}", x);

        }

        static void showNoInTriangleByUserWidth()
        {

            Console.Write("Enter a number:");
            int number = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter the desired width:");
            int width = Convert.ToInt32(Console.ReadLine());
            int length = width;

            Console.WriteLine("Expected Output :");

            for (int row = 0; row < length; row++)
            {

                for (int col = 0; col < width; col++)
                {

                    Console.Write(number);
                }
                Console.WriteLine();
                width--;
            }

        }

        static void rightAngleTriangle()
        {
            Console.WriteLine("Expected Output :");
            Console.WriteLine("The pattern like :");
            int endValue = 1;

            for (int i = 1; i <= endValue; i++)
            {
                if (endValue == 5)
                {
                    break;
                }

                for (int j = 1; j <= endValue; j++)
                {
                    Console.Write(j);
                }
                Console.WriteLine();

                endValue++;
            }

        }

        static void makePyramid()
        {

            Console.Write("Enter rows :");
            int userRows = Convert.ToInt32(Console.ReadLine());

            int space = userRows + 1;
            int numberValue = 1;

            Console.WriteLine("Expected Output :");

            for (int i = 1; i <= userRows; i++)
            {

                for (int j = space; j > 1; j--)
                {

                    Console.Write(" ");
                }

                for (int k = 1; k <= i; k++)
                {

                    Console.Write(" " + numberValue++);

                }
                Console.WriteLine();
                space--;

            }

        }

        public static void loopAllMethod()
        {

            Console.WriteLine("Press 1 for loop 10 numbers");
            Console.WriteLine("Press 2 for sum of loop 10 numbers");
            Console.WriteLine("Press 3 for show numbers in rectangle");
            Console.WriteLine("Press 4 for show numbers in triangle");
            Console.WriteLine("Press 5 for right angle triangle");
            Console.WriteLine("Press 6 for make pyramid");
            Console.Write("Enter here :");
            int userChoice = Convert.ToInt32(Console.ReadLine());

            switch (userChoice)
            {
                case 1: loopNumbers(); break;
                case 2: sumLoopNumber(); break;
                case 3: showNoInRectang(); break;
                case 4: showNoInTriangleByUserWidth(); break;
                case 5: rightAngleTriangle(); break;
                case 6: makePyramid(); break;
            }

            Console.Write("Do you want to continue y/n :");

            string userChoose = Console.ReadLine();

            if (userChoose == "y")
            {
                loopAllMethod();
            }
            else
            {
                Console.WriteLine("\"Thanks\" for coming");
            }
        }
    }
}