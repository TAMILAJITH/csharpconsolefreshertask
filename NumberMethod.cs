using System;
using System.Linq;


namespace ConsoleApp
{
    public static class NumberMethod
    {

        // Print Hello World

        static void helloWorld()
        {

            Console.WriteLine("Hello World");
        }

        //Calculator
        static void Calculator()
        {


            Console.Write("Input the first Number :");
            int FirstOperant = Convert.ToInt32(Console.ReadLine());

            Console.Write("Input the second Number :");
            int SecondOperant = Convert.ToInt32(Console.ReadLine());



            Console.WriteLine("Expected Output : \n");
            Console.WriteLine("{0} + {1} = {2}", FirstOperant, SecondOperant, FirstOperant + SecondOperant);
            Console.WriteLine("{0} - {1} = {2}", FirstOperant, SecondOperant, FirstOperant - SecondOperant);
            Console.WriteLine("{0} * {1} = {2}", FirstOperant, SecondOperant, FirstOperant * SecondOperant);
            Console.WriteLine("{0} / {1} = {2}", FirstOperant, SecondOperant, FirstOperant / SecondOperant);
            Console.WriteLine("{0} % {1} = {2}", FirstOperant, SecondOperant, FirstOperant % SecondOperant);

        }

        // Get Average Number
        static void getAvgNo()
        {
            Console.Write("Enter the First Number :");
            int FirstOperant = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter the Second Number :");
            int SecondOperant = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter the Third Number :");
            int ThirdOperant = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter the Fourth Number");
            int FourthOperant = Convert.ToInt32(Console.ReadLine());

            int[] array = { FirstOperant, SecondOperant, ThirdOperant, FourthOperant };

            double average = Queryable.Average(array.AsQueryable());

            int Average = Convert.ToInt32(Math.Floor(average));

            Console.WriteLine("Expected output :\n");

            Console.WriteLine("The average of {0} , {1} , {2} , {3} is {4} :", FirstOperant, SecondOperant, ThirdOperant, FourthOperant, Average);


        }

        //get odd numbers from 1 to 99 .

        static void oddNumbers()
        {

            Console.Write("Output :");

            Console.WriteLine("Odd Numbers from 1 to 99");

            for (int i = 1; i <= 99; i++)
            {
                if (i % 2 != 0)
                {
                    Console.WriteLine(i.ToString());
                }
            }
        }

        //  check if a given positive number is a multiple of 3 or a multiple of 7. 

        static void checkGivenNumbers()
        {

            Console.Write("Input first Integer :");
            int x = Convert.ToInt32(Console.ReadLine());

            if (x > 0)
            {

                bool value = (x % 3 == 0 || x % 7 == 0);

                Console.WriteLine("Output : \n");

                Console.WriteLine(value);
            }
        }

        // find Largest and Lowest from given three number

        static void findMinAndMax()
        {

            Console.Write("Input first integar :");
            int first = Convert.ToInt32(Console.ReadLine());
            Console.Write("Input second integar :");
            int second = Convert.ToInt32(Console.ReadLine());
            Console.Write("Input third integar :");
            int third = Convert.ToInt32(Console.ReadLine());

            // int maxNumber =Math.Max(first,second);
            // int minNumber =Math.Min(first,second);

            Console.WriteLine("Output : \n");

            Console.WriteLine("Largest of Three :" + (Math.Max(first, Math.Max(second, third))));
            Console.WriteLine("Lowest of Three :" + (Math.Min(first, Math.Min(second, third))));

        }

        //find the largest of three numbers.

        static void getLargestNoWithIdentity()
        {


            Console.Write("Input the 1st number :");
            int first = Convert.ToInt32(Console.ReadLine());
            Console.Write("Input the 1st number :");
            int second = Convert.ToInt32(Console.ReadLine());
            Console.Write("Input the 1st number :");
            int third = Convert.ToInt32(Console.ReadLine());

            if (first > second && first > third)
            {
                Console.WriteLine("Output : \n");

                Console.WriteLine("1st number greater than among three");
            }
            else if (second > first && second > third)
            {
                Console.WriteLine("Output : \n");

                Console.WriteLine("2st number greater than among three");

            }
            else if (third > first && third > second)
            {
                Console.WriteLine("Output : \n");

                Console.WriteLine("3st number greater than among three");

            }


        }

        //get and set student Roll No,Name,Marks in subjects,Total Mark,Percentage and division.

        static void findStudentDetails()
        {

            Console.Write("Input the Roll Number of the student :");
            string rollNo = Console.ReadLine();
            Console.Write("Input the Name of the Student :");
            string name = Console.ReadLine();

            Console.Write("Input the marks of Physics :");
            int physicsMark = Convert.ToInt32(Console.ReadLine());

            Console.Write("Input the marks of Chemistry :");
            int chemistryMark = Convert.ToInt32(Console.ReadLine());

            Console.Write("Input the marks of Computer Application :");
            int computerApplicationMark = Convert.ToInt32(Console.ReadLine());

            int totalMark = (physicsMark + chemistryMark + computerApplicationMark);

            double calcPercentageOfMark = (totalMark / 3);
            int PercentageOfMark = Convert.ToInt32(Math.Round(calcPercentageOfMark));

            Console.WriteLine("Expected Output : \n");

            Console.WriteLine("Student Roll NO :" + rollNo);

            Console.WriteLine("Student Name :" + name);

            Console.WriteLine("Marks in Physics :" + physicsMark);
            Console.WriteLine("Marks in chemistry :" + chemistryMark);
            Console.WriteLine("Marks in computerApplication :" + computerApplicationMark);
            Console.WriteLine("Total :" + totalMark);
            Console.WriteLine("Percentage :" + PercentageOfMark + "%");

            if (PercentageOfMark >= 80)
            {
                Console.WriteLine("Division : First Class");
            }
            else if (PercentageOfMark >= 60 && PercentageOfMark <= 80)
            {
                Console.WriteLine("Division : Second Class");
            }
            else if (PercentageOfMark >= 36 && PercentageOfMark <= 60)
            {
                Console.WriteLine("Division : Pass");
            }
            else
            {
                Console.WriteLine("Division : Fail");
            }
        }

        public static void loopAllMethods()
        {

            Console.WriteLine("Press 1 for Calculate No");
            Console.WriteLine("Press 2 for get Avg No");
            Console.WriteLine("Press 3 for get odd No");
            Console.WriteLine("Press 4 for check given No");
            Console.WriteLine("Press 5 for find min and max No");
            Console.WriteLine("Press 6 for get largest No");
            Console.WriteLine("Press 7 for find student detail");
            Console.Write("Enter here :");

            int userChoice = Convert.ToInt32(Console.ReadLine());

            switch (userChoice)
            {

                case 1: Calculator(); break;
                case 2: getAvgNo(); break;
                case 3: oddNumbers(); break;
                case 4: checkGivenNumbers(); break;
                case 5: findMinAndMax(); break;
                case 6: getLargestNoWithIdentity(); break;
                case 7: findStudentDetails(); break;
                default: Console.WriteLine("Please choose valid Number"); break;
            }

            Console.Write("Do you want to continue y/n :");

            string userChoose = Console.ReadLine();

            if (userChoose == "y")
            {
                loopAllMethods();
            }
            else
            {
                Console.Write("\"Thanks\" please come return when you desire ");
            }





        }


    }
}