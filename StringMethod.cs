using System;

namespace ConsoleApp
{
    public static class StringMethod
    {

        // Convert To Lowercase 

        static void toLowerCase()
        {

            Console.Write("Please enter your name in any case");
            string name = Console.ReadLine();

            Console.WriteLine("Expected Output :\n");
            Console.WriteLine("LowerValue :" + name.ToLower());
        }

        //find the longest word in a string.
        static void largestWord()
        {

            Console.Write("Please enter the sentence");

            string sentence = Console.ReadLine();

            string[] convertToArray = sentence.Split(" ");

            string largestWordValue = "";

            int lengthOfString = 0;

            for (int i = 0; i < convertToArray.Length; i++)
            {

                if (convertToArray[i].Length > lengthOfString)
                {

                    largestWordValue = convertToArray[i];

                    lengthOfString = convertToArray[i].Length;
                }
            }
            Console.WriteLine("Expected Output :\n");

            Console.WriteLine("Largest Word :" + largestWordValue);

        }

        // Reverse the Sentence

        static void reverseSentence()
        {

            Console.Write("Please enter sentence");
            string value = Console.ReadLine();

            Console.WriteLine("Expected Output :\n");

            string[] splitValue = value.Split(" ");

            Console.WriteLine("Original String :");

            foreach (string item in splitValue)
            {
                Console.Write(item + ' ');
            }
            Console.WriteLine();
            Array.Reverse(splitValue);

            Console.WriteLine("Reverse String :");

            foreach (string item in splitValue)
            {

                Console.Write(item + ' ');
            }

        }

        //Reverse order Letter

        static void reverseOrder()
        {

            Console.Write("please enter first letter :");
            string firstLetter = Console.ReadLine();

            Console.Write("please enter second letter :");
            string secondLetter = Console.ReadLine();

            Console.Write("please enter third letter :");
            string thirdLetter = Console.ReadLine();

            string[] arrayValue = { firstLetter, secondLetter, thirdLetter };

            Array.Reverse(arrayValue);

            Console.WriteLine("Expected Output :");

            foreach (string item in arrayValue)
            {

                Console.Write(item + ' ');
            }

        }

        // Check Whether an Alphabet
        static void checkAnAlphabet()
        {

            Console.Write("Enter Alphabet :");
            char character = Char.Parse(Console.ReadLine());

            if (Char.IsLetter(character))
            {

                switch (character)
                {

                    case 'a': Console.WriteLine(character + " " + "is a Vowel"); break;
                    case 'A': Console.WriteLine(character + " " + "is a Vowel"); break;
                    case 'e': Console.WriteLine(character + " " + "is a Vowel"); break;
                    case 'E': Console.WriteLine(character + " " + "is a Vowel"); break;
                    case 'i': Console.WriteLine(character + " " + "is a Vowel"); break;
                    case 'I': Console.WriteLine(character + " " + "is a Vowel"); break;
                    case 'o': Console.WriteLine(character + " " + "is a Vowel"); break;
                    case 'O': Console.WriteLine(character + " " + "is a Vowel"); break;
                    case 'u': Console.WriteLine(character + " " + "is a Vowel"); break;
                    case 'U': Console.WriteLine(character + " " + "is a Vowel"); break;
                    default: Console.WriteLine(character + " " + "is Consonant"); break;

                }
            }
            else
            {
                Console.WriteLine("Entered value not Alphabet");
            }
        }


        public static void loopAllMethods()
        {

            Console.WriteLine("Press 1 for lower case");
            Console.WriteLine("Press 2 for Largest word");
            Console.WriteLine("Press 3 for reverse sentence");
            Console.WriteLine("Press 4 for reverse order");
            Console.WriteLine("Press 5 for check and alphabet");
            Console.Write("Enter here :");
            int userChoice = Convert.ToInt32(Console.ReadLine());

            switch (userChoice)
            {
                case 1: toLowerCase(); break;
                case 2: largestWord(); break;
                case 3: reverseSentence(); break;
                case 4: reverseOrder(); break;
                case 5: checkAnAlphabet(); break;
                default: Console.WriteLine("Please enter valid No"); break;
            }

            Console.Write("Do you want to continue y/n :");
            string userChoose = Console.ReadLine();
            if (userChoose == "y")
            {
                loopAllMethods();
            }
            else
            {
                Console.WriteLine("\"Thanks\" for Coming");
            }



        }

    }
}